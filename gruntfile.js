module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        karma: {
            unit: {
                configFile: 'etc/karma.conf.js'
            }
        },
        bower: {
            install: {
                options: {
                    targetDir: 'lib',
                    layout: 'byComponent',
                    //layout: 'byType',
                    install: true,
                    verbose: false,
                    cleanTargetDir: true,
                    cleanBowerDir: false
                }
            }
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: 'src',
                    mainConfigFile: 'src/require.config.js',
                    include: [
                        'Isobel.js'
                    ],
                    exclude: [
                        'easeljs',
                        'preloadjs',
                        'underscore'
                    ],
                    out: 'dist/isobel.min.js'
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-bower-task');

    grunt.registerTask('default', ['karma']);
    grunt.registerTask('build', ['requirejs']);
};
