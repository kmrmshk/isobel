// src/require.config.js
require.config({
    baseUrl: 'src',
    paths: {
        'baseClass': 'lib/base-class-extend',
        'easeljs': 'vendor/EaselJS/lib/easeljs-0.8.2.min',
        'preloadjs': 'vendor/PreloadJS/lib/preloadjs-0.6.2.min',
        'underscore': 'vendor/underscore/underscore'
    },
    shim: {
        'baseClass': {
            exports: 'BaseClass'
        },
        'underscore': {
            exports: '_'
        }
    }
});