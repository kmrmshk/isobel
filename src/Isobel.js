/*!
 * Isobel
 * A Isometric rendering engine.
 */
define([
    'isobel/Block',
    'isobel/Point',
    'isobel/Layer'
], function(
    Block,
    Point,
    Layer
){

    var Isobel = Layer.extend(
        {
            constructor: function Isobel(mapData){
                var _mapData = mapData;
            }
        },
        {
            Point: Point,
            Block: Block,
            Layer: Layer
        }
    );
    return Isobel;
});
