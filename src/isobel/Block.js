define([
    'baseClass',
    'isobel/Point'
], function(BaseClass, Point){

    var Block = Point.extend('Block', {
        constructor: function Block(width, length, height, point){
            Point.call(this, point.x, point.y, point.z);
            var _width = width;
            var _length = length;
            var _height = height;
            this.addPrototype({
                get width(){
                    return _width;
                },
                set width(value){
                    _width = value;
                },
                get length(){
                    return _length;
                },
                set length(value){
                    _length = value;
                },
                get height(){
                    return _height;
                },
                set height(value){
                    _height = value;
                },
            });
        }
    });
    return Block;
})