define([
    'baseClass'
], function(BaseClass){

    var Point = BaseClass.extend('Point', {
        constructor: function Point(x, y, z){
            var _x = parseInt(x, 10);
            var _y = parseInt(y, 10);
            var _z = parseInt(z, 10);
            this.addPrototype({
                get x(){
                    return _x;
                },
                set x(value){
                    _x = parseInt(value, 10);
                },
                get y(){
                    return _y;
                },
                set y(value){
                    _y = parseInt(value, 10);
                },
                get z(){
                    return _z;
                },
                set z(value){
                    _z = parseInt(value, 10);
                },
            });
        },
        clone: function(){
            return new Point(this.x, this.y, this.z);
        }
    });
    return Point;
})