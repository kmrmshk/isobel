// test/require.config.js
var tests = [];
for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (/Spec\.js$/.test(file)) {
            tests.push(file);
        }
    }
}

require.config({
    baseUrl: '/base/src',
    paths: {
        'baseClass': 'lib/base-class-extend',
        'easeljs': 'vendor/EaselJS/lib/easeljs-0.8.2.min',
        'preloadjs': 'vendor/PreloadJS/lib/preloadjs-0.6.2.min',
        'underscore': 'vendor/underscore/underscore'
    },
    shim: {
        'baseClass': {
            exports: 'BaseClass'
        },
        'underscore': {
            exports: '_'
        }
    },
    deps: tests,
    callback: window.__karma__.start
});