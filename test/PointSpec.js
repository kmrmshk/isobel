define([
    'Isobel'
], function(Isobel){
    'use strict';

    describe('Isobel', function(){
        beforeEach(function(){

        });

        afterEach(function(){
            // no options
        });

        // Isobel
        it('Isobel is Defined', function(){
            var isobel = new Isobel;
            expect(isobel).toBeDefined();
        });

        // Point
        it('Isobel has Point class', function(){
            expect(Isobel.Point).toBeDefined();
        });

        // Block
        it('Isobel has Block class', function(){
            expect(Isobel.Block).toBeDefined();
        });

        // Layer
        it('Isobel has Layer class', function(){
            expect(Isobel.Layer).toBeDefined();
        });



    });
});